#!/usr/bin/env bash

E_bold="\e[1m"      # bold style
E_normal="\e[0m"    # reset style
E_red="\e[31m"      # text color: red
E_green="\e[32m"    # text color: green

padding='============================================================'
padlength="${#padding}"

rval='0'

if ! type -P shellcheck &> /dev/null; then
    printf '%bERROR: please install shellcheck%b\n' "${E_bold}${E_red}" "${E_normal}"
    exit 1
fi

if [[ "${SH_DEBUG}" == true ]]; then
    set -x
fi

check() {
    header=" Check ${1} "
    shift

    sc_args="$*"
    shift $#
    printf '%0.3s' "${padding}"
    printf '%b%s%b' "${E_bold}" "${header}" "${E_normal}"
    printf '%*.*s\n' 0 $((padlength - ${#header} -3 )) "${padding}"

    # shellcheck disable=SC2086
    if shellcheck --color=always ${sc_args}; then
        printf '%b=== OK ' "${E_green}"
    else
        rval="$((rval + 1))"
        printf '%b=======' "${E_red}"
    fi

    printf '%*.*s%b\n\n' 0 "$((padlength - 7))" "${padding}" "${E_normal}"
}

repo_path="$(git rev-parse --show-toplevel)"
cd "${repo_path}" || { printf 'ERROR: repo path '\''%s'\'' not found' "${repo_path}"; exit 1; }

grep -rl '^#!/usr/bin/env' | while read -r script; do
#find bin -type f | while read -r script; do
    if [ -f "${script}" ]; then
        check "${script}" "${script}"
    fi
done


exit "${rval}"
