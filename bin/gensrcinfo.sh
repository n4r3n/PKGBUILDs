#!/usr/bin/env bash

E_bold="\e[1m"      # bold style
E_normal="\e[0m"    # reset style
E_red="\e[31m"      # text color: red
E_green="\e[32m"    # text color: green

padding='============================================================'
padlength="${#padding}"

rval='0'

if ! type -P makepkg &> /dev/null; then
    printf '%bERROR: please install makepkg / pacman%b\n' "${E_bold}${E_red}" "${E_normal}"
    exit 1
fi

if [[ "${SH_DEBUG}" == true ]]; then
    set -x
fi

generate() {
    header=" Generate hashes and SRCINFO file for ${1#*/} "
    cd "${1}" || { rval="$((rval + 1))"; printf '%b=======' "${E_red}"; }

    printf '%0.3s' "${padding}"
    printf '%b%s%b' "${E_bold}" "${header}" "${E_normal}"
    printf '%*.*s\n' 0 $((padlength - ${#header} -3 )) "${padding}"

    # shellcheck disable=SC2086
#    if updpkgsums &> /dev/null && makepkg --printsrcinfo > .SRCINFO; then
    if makepkg --printsrcinfo > .SRCINFO; then
        printf '%b=== OK ' "${E_green}"
    else
        rval="$((rval + 1))"
        printf '%b=======' "${E_red}"
    fi

    printf '%*.*s%b\n\n' 0 "$((padlength - 7))" "${padding}" "${E_normal}"
}

repo_path="$(git rev-parse --show-toplevel)"
cd "${repo_path}" || { printf 'ERROR: repo path '\''%s'\'' not found' "${repo_path}"; exit 1; }

find . -name PKGBUILD | while read -r pkgbuild; do
    if [ -f "${pkgbuild}" ]; then
        (
        generate "${pkgbuild%/*}"
        )
    fi
done

exit "${rval}"
