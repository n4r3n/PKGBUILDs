#!/usr/bin/env bash

if "${SH_DEBUG:-false}"; then
    set -x
fi

repo_home="$(git rev-parse --show-toplevel)"
git_hook="${repo_home}/.git/hooks"
hook_path="${repo_home}/bin/git-hooks"

if [[ -z "${repo_home}" ]]; then
    exit 1
fi

# shellcheck disable=SC2164
cd "${hook_path}"
for hook in *; do
    if [[ ! -e "${git_hook}/${hook}" ]]; then
        ln -s "../../bin/git-hooks/${hook}" "${git_hook}/${hook}" &&
            printf 'Symlink for %s created\n' "${hook}"
    fi
done
